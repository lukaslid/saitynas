module.exports = {
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
        {
          test: /\.css$/,
          // exclude: /node_modules/,
          use: [
            {
              // Adds CSS to the DOM by injecting a `<style>` tag
              loader: 'style-loader'
            },
            {
              // Interprets `@import` and `url()` like `import/require()` and will resolve them
              loader: 'css-loader'
            },
          ]
        },
            // images and fonts
        {
          test: /\.(gif|ttf|eot|svg|woff2?)$/, 
          use: 'url-loader?name=[name].[ext]'
        },
      ]
    }
  };