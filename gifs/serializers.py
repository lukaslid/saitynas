import os

from django.contrib.auth.models import User, Group
from rest_framework import serializers
from gifs.models import GIF, Like, Comment, Tag
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView


class UserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(max_length=25)
    password = serializers.CharField(write_only=True, max_length=30)

    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'email']

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.is_active = True
        user.save()
        return user

    def validate(self, data):
        username = data['username']

        if User.objects.filter(username=username).exists():
            raise serializers.ValidationError('Username already exists')
        return data


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class CommentSerializer(serializers.ModelSerializer):
    text = serializers.CharField(max_length=200)
    user = UserSerializer(many=False, read_only=True)

    class Meta:
        model = Comment
        fields = ['id', 'text', 'user', 'gif', 'created']
        extra_kwargs = {
            'id': {
                'required': False,
            },
            'user': {
                'required': False,
            },
            'created': {
                'required': False,
            }
        }


class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = ['id', 'gif', 'user', 'created']
        extra_kwargs = {
            'id': {
                'required': False,
            },
            'user': {
                'required': False,
            }
        }


class TagSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.CharField(max_length=50)
    value = serializers.CharField(source="name", required=False)
    title = serializers.CharField(source="id", required=False)

    class Meta:
        model = Tag
        fields = ['id', 'value', 'name', 'title', 'created']


class GIFSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=100, required=True)
    image = serializers.FileField(required=True)
    src = serializers.FileField(source='image', required=False)
    caption = serializers.CharField(source="name", required=False)
    thumbnail = serializers.FileField(source='image', required=False)
    tags = TagSerializer(many=True, read_only=True)
    likes = LikeSerializer(many=True, read_only=True)
    comments = CommentSerializer(many=True, read_only=True)
    user = UserSerializer(many=False, read_only=True)

    def validate(self, data):
        img = data.get('image', None)
        if not img:
            raise serializers.ValidationError("Media file is required")
        ext = os.path.splitext(img.name)[1]
        if ext != '.gif':
            raise serializers.ValidationError("File format not supported, must be a .gif")
        if img.size > 5e6:
            raise serializers.ValidationError("File must be not larger than 5MB")
        return data

    class Meta:
        model = GIF
        fields = ['id', 'caption', 'src', 'thumbnail', 'name', 'image', 'tags', 'likes', 'comments', 'user']
        extra_kwargs = {
            'id': {
                'required': False,
            },
            'image': {
                'required': False,
            },
            'likes': {
                'required': False,
            },
            'comments': {
                'required': False,
            },
        }


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # Add custom claims
        token['name'] = user.username
        token['id'] = user.id
        token['is_staff'] = user.is_staff

        return token

class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer