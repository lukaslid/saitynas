from django.urls import path
from gifs.views import *

router = routers.DefaultRouter()
router.register(r'api/users', views.UserViewSet)
router.register(r'api/comments', views.CommentViewSet)
router.register(r'api/gifs', views.GIFViewSet)
router.register(r'api/groups', views.GroupViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
]