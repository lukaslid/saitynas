from django.db import models
from django.contrib.auth.models import User, Group
from django.core.files import File
from urllib.request import urlopen
from tempfile import NamedTemporaryFile
from django.conf import settings
from django.core.files.storage import FileSystemStorage


class Tag(models.Model):
    name = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)


class GIF(models.Model):
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to='images')
    user = models.ForeignKey(User, related_name='gifs', on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    # def save(self, *args, **kwargs):
    #     super(GIF, self).save(*args, **kwargs)


class Like(models.Model):
    gif = models.ForeignKey(GIF, related_name='likes', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='likes', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class Comment(models.Model):
    gif = models.ForeignKey(GIF, related_name='comments', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='comments', on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='children', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
