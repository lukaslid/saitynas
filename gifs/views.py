import json
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from gifs.serializers import *
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework import renderers
from rest_framework import permissions
from rest_framework.decorators import action, permission_classes as permission
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from django_filters import rest_framework
import json


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

    def list(self, request, *args, **kwargs):
        if not self.request.user.is_staff:
            return Response({'message': "Forbidden"}, status=403)
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=True)
    @permission([IsAuthenticated])
    def gifs(self, request, *args, **kwargs):
        user = self.get_object()
        gifs = user.gifs.all()
        return Response(GIFSerializer(gifs, many=True).data)

    @action(detail=False, methods=["post"])
    @permission([IsAdminUser])
    def staff(self, request, *args, **kwargs):
        if not self.request.user.is_staff:
            return Response({'message': "Forbidden"}, status=403)
        user_id = self.request.data.get('user')
        user = User.objects.filter(id=user_id)
        user.update(is_staff=True)
        return Response({'message': "Updated"})

    def update(self, request, *args, **kwargs):
        if not self.request.user.is_staff:
            return Response({'message': "Forbidden"}, status=403)
        instance = self.get_object()
        self.perform_update(instance)
        return Response({'message': "Success"})

    def destroy(self, request, *args, **kwargs):
        if not self.request.user.is_staff:
            return Response({'message': "Forbidden"}, status=403)
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({'message': "Success"})


class GIFViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    parser_classes = [MultiPartParser]
    queryset = GIF.objects.all().order_by('-created')
    serializer_class = GIFSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    @action(detail=True)
    def tags(self, request, *args, **kwargs):
        gif = self.get_object()
        tags = gif.tags.all()
        return Response(TagSerializer(tags, many=True).data)

    @action(detail=True)
    def comments(self, request, *args, **kwargs):
        gif = self.get_object()
        comments = gif.comments.all()
        return Response(CommentSerializer(comments, many=True).data)

    @action(detail=True)
    def likes(self, request, *args, **kwargs):
        gif = self.get_object()
        likes = gif.likes.all()
        return Response(LikeSerializer(likes, many=True).data)

    def perform_create(self, serializer):
        array = json.loads(self.request.data.get('tags'))
        f = self.request.FILES.get('image', None)
        serializer.save(user=self.request.user, image=f, tags=array)

    def update(self, request, *args, **kwargs):
        if not self.request.user.is_staff:
            return Response({'message': "Forbidden"}, status=403)
        instance = self.get_object()
        self.perform_update(instance)
        return Response({'message': "Success"})

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if self.request.user != instance.user or not self.request.user.is_staff:
            return Response({'message': "Forbidden"}, status=403)
        self.perform_destroy(instance)
        return Response({'message': "Success"})


class CommentViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = Comment.objects.all().order_by('-created')
    serializer_class = CommentSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user, gif=GIF(self.request.data.get('gif')))

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        if self.request.user != instance.user or not self.request.user.is_staff:
            return Response({'message': "Forbidden"}, status=403)
        self.perform_update(instance)
        return Response({'message': "Success"})

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if self.request.user != instance.user or not self.request.user.is_staff:
            return Response({'message': "Forbidden"}, status=403)
        self.perform_destroy(instance)
        return Response({'message': "Success"})


class LikeViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = Like.objects.all()
    serializer_class = LikeSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        gif = GIF(self.request.data.get('gif'))
        if self.queryset.filter(user=self.request.user, gif=gif).exists():
            return Response({'message': "You can like only once"}, status=401)
        serializer.save(user=self.request.user, gif=gif)
        return Response({'message': "Success"})

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if self.request.user != instance.user:
            return Response({'message': "Forbidden"}, status=403)
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({'message': "Success"})


class TagViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        if not self.request.user.is_staff:
            return Response({'message': "Forbidden"}, status=403)
        serializer.save(user=self.request.user)

    def update(self, request, *args, **kwargs):
        if not self.request.user.is_staff:
            return Response({'message': "Forbidden"}, status=403)
        instance = self.get_object()
        self.perform_update(instance)
        return Response({'message': "Success"})

    def destroy(self, request, *args, **kwargs):
        if not self.request.user.is_staff:
            return Response({'message': "Forbidden"}, status=403)
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({'message': "Success"})
