import React from "react";
import ReactDOM from "react-dom";
import DataProvider from "./DataProvider";
import Table from "./Table";
import Form from "./Form"
import GifGallery from "./GifGallery"
import GifPage from "./GifPage"
import MyNavbar from "./MyNavbar";
import Login from "./Login";
import Logout from "./Logout";
// import Register from "./Register";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import AuthService from '../AuthService';
import Footer from './Footer';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
  
import 'react-widgets/dist/css/react-widgets.css';
import Register from "./Register";
AuthService.isAuthenticated()

const App = () => (
    <React.Fragment>
        <Container>
            <Router>
            <Row>
                <Col sm><MyNavbar user={AuthService.user }/></Col>
            </Row>
                <Switch>
                    <Route path="/login">
                        <Row>
                            <Col sm>  
                                <Login></Login>
                            </Col>
                        </Row>
                    </Route>
                    <Route path="/tags">
                        <DataProvider endpoint="api/tags/" 
                            render={data => <Table data={data} />} />
                        <Form endpoint="api/tags/" />
                    </Route>
                    <Route path="/users">
                        <DataProvider endpoint="api/users/" 
                            render={data => <Table data={data} />} />
                    </Route>
                    <Route path="/gifs/:id" component={GifPage}>
                    </Route>
                    <Route path="/logout" component={Logout}>
                    </Route>
                    <Route path="/register" component={Register}>
                    </Route>
                    <Route path="/">
                        <Row>
                            <Col sm>  
                                <DataProvider endpoint="api/gifs/" 
                                    render={data => <GifGallery data={data} />} />
                            </Col>
                        </Row>
                    </Route>
                </Switch>
                <Row>
                    <Col sm><Footer/></Col>
                </Row>
            </Router>
        </Container>
    </React.Fragment>
  );
  
const wrapper = document.getElementById("app");
wrapper ? ReactDOM.render(<App />, wrapper) : null;