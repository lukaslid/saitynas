import React, { Component } from "react";
import axios from "axios";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import styles from '../../static/styles/main.css'; 
import Comment from "./Comment"
import AuthService from '../AuthService';
import { withRouter } from 'react-router-dom';

class CommentSection extends Component {
    constructor(props){
        super(props);
        this.state = {
            commentText: '',
        }
        this.commentPost = this.commentPost.bind(this);
        this.onChangeHandler = this.onChangeHandler.bind(this);
    }

    commentPost(e) {
        const data = new FormData();
        data.append('text', this.state.commentText);
        data.append('gif', this.props.gifId)
        axios.post('http://localhost:8000/api/comments/', data, {
            headers: AuthService.getAuthHeader()
        }).then((response) => {
            this.state.commentText = ''
            this.props.update_page()
          })
          .catch((error) => {
            alert('error ' + error)
          });  
    }

    onChangeHandler = (e) => {
        this.setState({ commentText: e.target.value });
    }

    render () {
        return (
            <div className="col-md-12 col-md-offset-4 col-sm-12 pt-5">
                <div className="comment-wrapper">
                    <div className="panel panel-info">
                        <div className="panel-heading text-center">
                            <h3>Comment Section</h3>
                        </div>
                        <div className="panel-body">
                    
                            <textarea value={this.state.commentText} onChange={this.onChangeHandler} className="form-control" placeholder="write a comment..." rows="3"></textarea>
                            <br/>
                            <button type="button" className="btn btn-info float-right" onClick={this.commentPost}>Post</button>
                            <div className="clearfix"></div>
                            <hr/>
                            <ul className="media-list">
                                {
                                    this.props.comments.map(function(c){
                                        return <Comment comment={c} key={c.id} update_page={this.props.update_page}/>;
                                    }, this)
                                }
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

// const styles = {
//     container: {
//       justifyContent: 'center',
//       alignItems: 'center',
//     },
//     img_max: {
//         maxWidth: 500,
//         width:600,
//     },
//   };

export default withRouter(CommentSection);
