import React, { Component } from "react";
import PropTypes from "prop-types";
import Gallery from 'react-grid-gallery';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import GifForm from "./GifForm"
import DataProvider from "./DataProvider";
import axios from 'axios'
import AuthService from '../AuthService';
import { withRouter } from 'react-router-dom';

class GifGallery extends React.Component { 
    constructor(props){
        super(props);

        this.state = {
            images: this.props.data,
            showModal: false,
            selected_tags: [],
            current: null
        };
        this.close = this.close.bind(this);
        this.open = this.open.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.tagChangeHandler = this.tagChangeHandler.bind(this);
        this.fileUploadHandler = this.fileUploadHandler.bind(this);
        this.gifClickHandler = this.gifClickHandler.bind(this);
    }

    close(){
        this.setState({ showModal: false });
    }
    
    open(){
        this.setState({ showModal: true });
    }

    onChangeHandler = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    fileUploadHandler = (e) => {
        this.setState({image: e.target.files})
    }

    tagChangeHandler = (e) => {
        this.setState({ selected_tags: e});
    }

    gifClickHandler = (e) => {
        this.props.history.push({
            pathname: '/gifs/' + this.state.images[e].id
          })
        this.props.history.go()
    }

    handleSubmit(event) {
        event.preventDefault();

        const data = new FormData();
        data.append('name', this.state.title);
        data.append('image', this.state.image[0]);
        let tags = this.state.selected_tags.map(a => a.id);
        data.append('tags', JSON.stringify(tags));
        axios.post('http://localhost:8000/api/gifs/', data, {
            headers: AuthService.getAuthHeader()
        }).then((response) => {
            this.close();
            this.props.history.go(0)
          })
          .catch((error) => {
            alert('error ' + error)
          });    
    }

    render () {
        return (
                <div style={{
                    display: "block",
                    minHeight: "1px",
                    width: "100%",
                    overflow: "auto"}}>
                <Button variant="info" className="mt-4 mb-2" onClick={this.open}>Add new gif</Button>
                <Gallery
            images={this.state.images}
            enableLightbox={false}
            enableImageSelection={false}
            onClickThumbnail={this.gifClickHandler}/>

                <Modal show={this.state.showModal} onHide={this.close}>
                <Modal.Header closeButton>
                    <Modal.Title>Add gif</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <DataProvider endpoint="api/tags/" 
            render={data => <GifForm data={data} selected_tags={this.state.selected_tags} onChangeHandler={this.onChangeHandler}
                tagChangeHandler={this.tagChangeHandler} fileUploadHandler={this.fileUploadHandler}  />} />
                </Modal.Body>
                <Modal.Footer>
                    <Button type="submit"  onClick={this.handleSubmit}>Save</Button>
                    <Button variant="danger" onClick={this.close}>Close</Button>
                </Modal.Footer>
                </Modal>
                </div>
        );
    }
}

GifGallery.propTypes = {
    data: PropTypes.arrayOf(
        PropTypes.shape({
            src: PropTypes.string.isRequired,
            thumbnail: PropTypes.string.isRequired,
            srcset: PropTypes.array,
            caption: PropTypes.string,
            thumbnailWidth: 360,
            thumbnailHeight: 360
        })
    ).isRequired
};


export default withRouter(GifGallery);
