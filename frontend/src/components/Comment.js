import React, { Component } from "react";
import axios from "axios";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import styles from '../../static/styles/main.css'; 
import Icon from '@material-ui/core/Icon';
import AuthService from '../AuthService';
import { withRouter } from 'react-router-dom';

class Comment extends Component {
    constructor(props){
        super(props);
        var m = new Date(this.props.comment.created)
        var dateString = m.getUTCFullYear() +"/"+ (m.getUTCMonth()+1) +"/"+ m.getUTCDate() + " " + m.getUTCHours() + ":" + m.getUTCMinutes() + ":" + m.getUTCSeconds()
        var auth = AuthService.isAuthenticated()
        this.state = {
            date: dateString,
            commentText: this.props.comment.text,
            isWritable: false,
            canChange: auth && ((this.props.comment.user.id == AuthService.user.id) || AuthService.user.is_staff)
        }
        
        this.onChangeHandler = this.onChangeHandler.bind(this)
        this.editComment = this.editComment.bind(this)
        this.deleteComment = this.deleteComment.bind(this)
    }

    onChangeHandler = (e) => {
        this.setState({ commentText: e.target.value });
    }

    editComment() {
        const data = new FormData();
        data.append('text', this.state.commentText);
        data.append('gif', this.props.comment.gif)
        axios.put('http://localhost:8000/api/comments/' + this.props.comment.id + "/", data, {
            headers: AuthService.getAuthHeader()
        }).then((response) => {
            this.setState({isWritable: false})
          })
          .catch((error) => {
            alert('error ' + error)
          });  
    }

    deleteComment() {
        if (confirm("Delete this comment?")){
        axios.delete('http://localhost:8000/api/comments/' + this.props.comment.id + "/", {
            headers: AuthService.getAuthHeader()
        })
        .then(() => {
            this.props.update_page()
        })
        .catch((error) => {
            alert('error ' + error)
          });  
        }
    }

    render () {
        return (
        <div>
            <li className="media">
                <div className="media-body">
                    <span className="text-muted pull-right">
                        
                        <small className="text-muted">{this.state.date}</small>
                    </span>
                    <strong className="text-success">@{this.props.comment.user.username}</strong>
                    { !this.state.isWritable ?
                    <> {this.state.canChange &&
                        <>
                            <Icon v className="fas fa-trash float-right pr-5" color="secondary" onClick={this.deleteComment}></Icon>
                            <Icon className="fas fa-edit float-right pr-5" onClick={() => this.setState({isWritable: true })}></Icon>
                        </>
                        }
                        <p>
                            {this.state.commentText}
                        </p>
                    </>
                    :
                    <>
                        <textarea value={this.state.commentText} onChange={this.onChangeHandler} className="form-control" rows="3"></textarea>
                        <button type="button" className="btn btn-info float-right m-2" onClick={this.editComment}>Save</button>
                    </>
                    }

               </div>
             </li>
        </div>
        );
    }
}
export default withRouter(Comment);
