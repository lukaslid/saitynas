import React, { Component, ReactFragment } from "react";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import AuthService from '../AuthService';

class MyNavbar extends Component {
    constructor(props){
        super(props);
        this.state = {
            is_staff: false,
            loaded: false,
        }
    }
    
    componentDidMount(){
        if (this.props.user) {
            this.setState({is_staff: this.props.user.is_staff})
        }
        this.setState({loaded: true})
    }

    render () {
        return this.state.loaded && (
            <Navbar collapseOnSelect expand="lg">
            <Navbar.Brand href="/">Gifzz</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                                        { 
                    this.state.is_staff &&
                    <>
                    <Nav.Link href="/users">Users</Nav.Link>
                    <Nav.Link href="/tags">Tags</Nav.Link>
                    </>
                }
                    </Nav>
                <Nav>
                { 
                this.props.user ? 
                    <Nav.Link eventKey={1} href="/logout">
                        Log out
                    </Nav.Link>
                :
                <>
                    <Nav.Link href="/register">Register</Nav.Link>
                    <Nav.Link eventKey={2} href="/login">
                        Log in
                    </Nav.Link>
                </>
                }
                </Nav>
            </Navbar.Collapse>
            </Navbar>
        );
    }
}

export default MyNavbar;
