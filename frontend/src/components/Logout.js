import React from 'react';
import AuthService from '../AuthService';
import { withRouter } from 'react-router-dom';

class Logout extends React.Component {
    constructor(props){
        super(props);
    }

    componentDidMount() {
        AuthService.logout();
        this.props.history.push('/')
    }
    render () {
        return null;
    }

}

export default withRouter(Logout);