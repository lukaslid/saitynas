import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { withRouter } from 'react-router-dom';
import axios from "axios";

class Register extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            username: '',
            password: '',
            password2: '',
            message: {
                username: null,
                password: null,
                non_field_error: null
            }
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        localStorage.clear();
    }

    // register = (e) => {
    //     e.preventDefault();
    //     if ()
    //     const credentials = {username: this.state.username, password: this.state.password};
    //     AuthService.login(credentials).then(res => {
    //         if(res.status === 200){
    //             localStorage.setItem("userInfo", JSON.stringify(res.data.access));
    //             localStorage.setItem("userRefresh", JSON.stringify(res.data.refresh));
    //             AuthService.setUser(JSON.stringify(res.data.access))
    //             this.props.history.push('/');
    //         }
    //     }).catch((error => {
    //         console.log(error.response)
    //         this.setState({message: error.response})
    //     }));
    // }

    handleSubmit(event) {
        event.preventDefault();
        if (this.state.password !== this.state.password2){
            this.setState({message: "Password do not match"})
            return
        }

        const data = new FormData();
        data.append('username', this.state.username);
        data.append('password', this.state.password);
        axios.post('http://localhost:8000/api/users/', data).then((response) => {
            this.props.history.push('/')
          })
          .catch((error) => {
              this.setState({
                message: 
                    {
                        username: error.response.data.username,
                        password: error.response.data.password,
                        non_field_error: error.response.data.non_field_errors
                    }})
          });    
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return(
            <React.Fragment>

                <Container maxWidth="sm">
                <div style={styles.paper}>
                    <Typography variant="h4" style={styles.center}>Register</Typography>
                    <form style={{width: '100%'}}>
                        <TextField required type="text" label="USERNAME" fullWidth margin="normal" name="username" value={this.state.username} onChange={this.onChange}/>
                        
                        <Typography style={styles.notification}>{this.state.message.username}</Typography>
                        
                        <TextField required type="password" label="PASSWORD" fullWidth margin="normal" name="password" value={this.state.password} onChange={this.onChange}/>
                       
                        <Typography style={styles.notification}>{this.state.message.password}</Typography>
                        
                        <TextField required type="password" label="REPEAT PASSWORD" fullWidth margin="normal" name="password2" value={this.state.password2} onChange={this.onChange}/>

                        <Button required variant="contained" color="secondary" onClick={this.handleSubmit}>Login</Button>
                        <Typography style={styles.notification}>{this.state.message.non_field_error}</Typography>
                    </form>
                </div>
                </Container>
            </React.Fragment>
        )
    }

}

const styles= {
    center :{
        display: 'flex',
        justifyContent: 'center'

    },
    notification: {
        display: 'flex',
        justifyContent: 'center',
        color: '#dc3545'
    },
    paper: {
        marginTop: '20%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      },
}

export default withRouter(Register);