import React, { Component } from "react";
import PropTypes from "prop-types";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Multiselect from 'react-widgets/lib/Multiselect'

class GifForm extends React.Component { 
    constructor(props) {
        super(props);
        this.state = {
            tags: this.props.data,
        };
        this.multiSelectChange = this.multiSelectChange.bind(this);
    }

    multiSelectChange = (e) => {
        console.log(e)
        this.setState({selected: e.id })
        console.log(this.state.selected)
        this.props.tagChangeHandler(this.state.selected)
    }
    
    render () {
        return (
            <Form>
            <Form.Group as={Row} controlId="title">
              <Form.Label column sm={3}>
                Title
              </Form.Label>
              <Col sm={9}> 
                <Form.Control type="text" name="title" placeholder="Title" value={this.props.title} onChange={this.props.onChangeHandler} />
              </Col>
            </Form.Group>
          
            <Form.Group as={Row}>
                <Form.Label column sm={3}>Select a gif</Form.Label>
                <Col sm={9}>
                    <Form.Control name="image" type="file" value={this.props.image} multiple onChange={this.props.fileUploadHandler}/>
                </Col>
            </Form.Group>

            <Form.Group controlId="tags" as={Row}>
                <Form.Label  column sm={3}>Select tags</Form.Label>
                <Col sm={9}>
                <Multiselect
                    name="tags"
                    value={this.props.selected_tags}
                    onChange={this.props.tagChangeHandler}
                    data={this.state.tags}
                    textField='value'
                    caseSensitive={true}
                    minLength={3}
                    filter='contains'
                    />
                    {/* <Form.Control as="select" multiple>
                        {this.state.tags.map((tag) => <option key={tag.id} value={tag.id}>{tag.value}</option>)}
                    </Form.Control> */}
                </Col>
            </Form.Group>
          </Form>
        );
    }
}

GifForm.propTypes = {
    data: PropTypes.array.isRequired
  };

export default GifForm;
