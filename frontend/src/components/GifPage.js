import React, { Component } from "react";
import axios from "axios";
import CommentSection from "./CommentSection"
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Image from 'react-bootstrap/Image';
import { StylesContext } from "@material-ui/styles/StylesProvider";
import Icon from '@material-ui/core/Icon';
import AuthService from '../AuthService';

class GifPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            id: this.props.match.params.id,
            loaded: false,
            liked: false, 
        };
        this.fetch = this.fetch.bind(this)
        this.upvote = this.upvote.bind(this)
        this.undoUpvote = this.undoUpvote.bind(this)
        this.deleteGif = this.deleteGif.bind(this)
    }

    componentDidMount() {
        this.fetch()
    }

    fetch(){
        axios.get('http://localhost:8000/api/gifs/' + this.state.id + '/')
        .then((response) => {
            AuthService.isAuthenticated()
            this.setState({ image: response.data});
            if (AuthService.isAuthenticated()){
                if (this.state.image.likes.some(e => e.user === AuthService.user.id)) {
                    this.setState({
                        liked: true
                    })
                }
            }
            else {
                this.setState({
                    liked: false
                })

            }
            this.setState({
                loaded: true
            })

          })
          .catch((error) => {
            console.log(error)
          })
    }

    upvote(){
        const data = new FormData();
        data.append('gif', this.state.image.id)
        axios.post('http://localhost:8000/api/likes/', data, {
            headers: AuthService.getAuthHeader()
        }).then(() => {
            this.fetch()
          })
          .catch(() => {
            alert('You can only upvote once!')
          });  
    }

    undoUpvote(){
        var like = null
        if (this.state.liked)
            like = this.state.image.likes.filter(obj => {
                return obj.user === AuthService.user.id
            })[0];
        axios.delete('http://localhost:8000/api/likes/' + like.id + "/", {
            headers: AuthService.getAuthHeader()
        }).then(() => {
            this.fetch()
            this.setState({
                liked: false
            })
          })
    }

    deleteGif() {
        if (confirm("Delete this gif?")){
        axios.delete('http://localhost:8000/api/gifs/' + this.state.image.id + "/", {
            headers: AuthService.getAuthHeader()
        })
        .then(() => {
            this.props.history.push('/')
        })
        .catch((error) => {
            alert('error ' + error)
          });  
        }
    }

    render () {
        return this.state.loaded ? (
            <div>   
                <Container>
                    <Row>
                        <div className="col-12 text-center m-2">
                            {
                            AuthService.isAuthenticated() && (AuthService.user.is_staff || AuthService.user.id === this.state.image.user.id) && 
                                <Icon className="fas fa-trash float-right pr-3" color="secondary" onClick={this.deleteGif}></Icon>
                            }
                            <h3>{this.state.image.name}</h3>
                        </div>
                    </Row>
                    <Row>
                        <div className="col-12">
                            <img className="mx-auto d-block img-fluid" src={this.state.image.src} style={styles.img_max}/>  
                        </div>
                        <div className="col-6 mt-4">
                            {this.state.liked ?
                            <Icon className="fas fa-heart float-right" color="secondary" style={{ fontSize: 30 }} onClick={this.undoUpvote}></Icon>
                            :
                            <Icon className="far fa-heart float-right" style={{ fontSize: 30 }}  onClick={this.upvote}></Icon>
                            }
                        </div>
                        <div className="col-6 mt-4 text-left">
                            <h2 styles="font-size: 50px">{this.state.image.likes.length}</h2>
                        </div>
                    </Row>
                    <Row>
                        <CommentSection comments={this.state.image.comments} gifId={this.state.image.id} update_page={this.fetch}></CommentSection>
                    </Row>

                </Container>
            </div>

        ): <p>Loading</p>;
    }
}

const styles = {
    container: {
      justifyContent: 'center',
      alignItems: 'center',
    },
    img_max: {
        max_width: 500,
        width:600,
    },
  };

export default GifPage;
