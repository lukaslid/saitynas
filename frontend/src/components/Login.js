import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import AuthService from '../AuthService';
import { withRouter } from 'react-router-dom';

class Login extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            username: '',
            password: '',
            message: {
                username: null,
                password: null,
                detail: null
            }
        }
        this.login = this.login.bind(this);
    }

    componentDidMount() {
        localStorage.clear();
        console.log('CLEARED')
    }

    login = (e) => {
        e.preventDefault();
        const credentials = {username: this.state.username, password: this.state.password};
        AuthService.login(credentials).then(res => {
            if(res.status === 200){
                localStorage.setItem("userInfo", JSON.stringify(res.data.access));
                localStorage.setItem("userRefresh", JSON.stringify(res.data.refresh));
                AuthService.setUser(JSON.stringify(res.data.access))
                this.props.history.push('/');
            }
        }).catch((error => {
            this.setState({
                message: 
                    {
                        username: error.response.data.username,
                        password: error.response.data.password,
                        detail: error.response.data.detail
                    }})
        }));
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return(
            <React.Fragment>

                <Container maxWidth="sm">
                <div style={styles.paper}>
                    <Typography variant="h4" style={styles.center}>Login</Typography>
                    <form style={{width: '100%'}}>
                        <TextField required type="text" label="USERNAME" fullWidth margin="normal" name="username" value={this.state.username} onChange={this.onChange}/>
                        <Typography style={styles.notification}>{this.state.message.username}</Typography>
                        <TextField required type="password" label="PASSWORD" fullWidth margin="normal" name="password" value={this.state.password} onChange={this.onChange}/>
                        <Typography style={styles.notification}>{this.state.message.password}</Typography>
                        <Button variant="contained" color="secondary" onClick={this.login}>Login</Button>
                        <Typography style={styles.notification}>{this.state.message.detail}</Typography>
                    </form>
                </div>
                </Container>
            </React.Fragment>
        )
    }

}

const styles= {
    center :{
        display: 'flex',
        justifyContent: 'center'

    },
    notification: {
        display: 'flex',
        justifyContent: 'center',
        color: '#dc3545'
    },
    paper: {
        marginTop: '20%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      },
}

export default withRouter(Login);