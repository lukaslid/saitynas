import axios from 'axios';
import cookie from 'react-cookies'
import { userInfo } from 'os';
var jwtDecode = require('jwt-decode');

const USER_API_BASE_URL = 'http://localhost:8000/api/';

class AuthService {
    user = null
    isAuthenticated() {
        if(localStorage.getItem('userInfo')) {
            this.user = jwtDecode(JSON.parse(localStorage.getItem("userInfo")));
            return true
        }
        return false
    }

    login(credentials){
        return axios.post(USER_API_BASE_URL + "login/", credentials);
    }

    getUserInfo(){
        return JSON.parse(localStorage.getItem("userInfo"));
    }

    setUser(token){
        this.user = jwtDecode(token);
    }

    getRefreshToken(){
        return JSON.parse(localStorage.getItem("userRefresh"));
    }

    getAuthHeader() {
        let info = this.getUserInfo()
        if (info)
            return {
                    Authorization: 'Bearer ' + this.getUserInfo(),
                    'X-CSRFToken': cookie.load('csrftoken')
                };
        else
            return {
                'X-CSRFToken': cookie.load('csrftoken')
            };
    }

    logout() {
        localStorage.removeItem("userInfo");
        localStorage.removeItem("userRefresh");
    }
}

export default new AuthService();