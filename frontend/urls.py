from django.urls import path, re_path
from . import views
urlpatterns = [
    path('', views.index ),
    # path('gif', views.index ),
    re_path(r'^gifs/(?P<id>[0-9]\d)/$', views.index),
    # path('gifs/*', views.index ),
    path('login', views.index ),
    path('users', views.index ),
    re_path(r'^(?!api|media).*$', views.index),
]