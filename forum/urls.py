from django.contrib import admin
from django.urls import include, path, re_path
from rest_framework import routers
from gifs import views
from django.conf.urls import include
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from django.conf import settings
from django.conf.urls.static import static
from gifs.serializers import MyTokenObtainPairView


router = routers.DefaultRouter()
router.register(r'api/users', views.UserViewSet)
router.register(r'api/comments', views.CommentViewSet)
router.register(r'api/gifs', views.GIFViewSet)
# router.register(r'api/groups', views.GroupViewSet)
router.register(r'api/likes', views.LikeViewSet)
router.register(r'api/tags', views.TagViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include('frontend.urls')),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls')),
    path('api/login/', MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('', include(static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)))
]
